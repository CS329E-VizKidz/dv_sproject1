require("jsonlite")
require("RCurl")
require("dplyr")
require("tidyr")
require("ggplot2")
require("ggthemes")
library("shiny")
library("shinydashboard")
require(leaflet)

df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from newcoder"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_cb38335', PASS='orcl_cb38335', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE), ))

server <- shinyServer(function(input, output){
  output$boxplot<-renderPlot({
    filtered <- df %>%
      dplyr::select(STUDENTDEBTOWE,GENDER, COUNTRYLIVE) %>%
      dplyr::filter(COUNTRYLIVE == input$country, STUDENTDEBTOWE != "null")%>%
      dplyr::mutate(REALSTUDENTDEBTOWE = as.numeric(as.character(STUDENTDEBTOWE)))
    ggplot(filtered,aes(x= filtered$GENDER, y = filtered$REALSTUDENTDEBTOWE, fill = GENDER)) + geom_boxplot() +
      labs(x = "GENDER", y = "AMOUNT OF STUDENT DEBT") + ylim(0,75000)
    })
  
  output$CP<-renderPlot({
    
    filtered <-df %>%
      dplyr::select(LANGUAGEATHOME, SCHOOLDEGREE) %>%
      dplyr::filter(LANGUAGEATHOME == input$language) %>% 
      dplyr::filter(LANGUAGEATHOME != "null", SCHOOLDEGREE != "null") %>%
      dplyr::group_by(LANGUAGEATHOME, SCHOOLDEGREE)%>%
      dplyr::summarize(n = n())
    
    ggplot(filtered,aes(x= factor(1), y = filtered$n, fill=filtered$SCHOOLDEGREE))  + 
      geom_bar(stat="identity")+ 
      theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
      coord_polar(theta = "y") + labs(x = " ", y = " ") +
      scale_fill_discrete(guide = guide_legend(title="SCHOOL DEGREE\n"))
  })
  
  output$plot<-renderPlot({
    filtered <-df %>%
      dplyr::select(AGE,JOBROLEINTEREST) %>%
      dplyr::filter(AGE==input$age, JOBROLEINTEREST != "null")%>%
      dplyr::group_by(AGE,JOBROLEINTEREST)%>%
      dplyr::summarize(count = n())%>%dplyr::arrange(desc(count))
    ggplot(filtered,aes(x= filtered$JOBROLEINTEREST, y = filtered$count, fill=filtered$JOBROLEINTEREST))  + geom_bar(stat="identity")+ theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position="none")+ylim(0,150) + labs(x = "JOB INTEREST", y = "COUNT")})
  
  output$summary<-renderPrint({
    filtered <-df %>%
      dplyr::select(AGE,JOBROLEINTEREST) %>%
      dplyr::filter(AGE!="null", JOBROLEINTEREST != "null")%>%
      dplyr::group_by(AGE, JOBROLEINTEREST) %>%
      dplyr::summarize(count = n())%>%dplyr::arrange(desc(count))
    summary(filtered)
  })
})

